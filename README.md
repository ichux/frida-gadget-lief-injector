# Frida Gadget Lief Injector

Automated script to inject frida gadget using ELF injection. The script is inspired by [https://lief.quarkslab.com/doc/latest/tutorials/09_frida_lief.html](09 - How to use frida on a non-rooted device) and the amazing work of [https://lief.quarkslab.com/doc/latest/index.html](LIEF Project).

[Read More!](https://jlajara.gitlab.io/posts/2019/05/18/Frida-non-rooted.html)

## Requirements

```
Python 3.6

pip install https://github.com/lief-project/packages/blob/lief-master-latest/pylief-0.9.0.dev.zip
pip install xtract
```

## Usage

```
python frida_lief_injection.py



[+] Enter the path of your APK: original.apk
[+] Unzip the original.apk in /tmp/tmpknzr8dx4_lief_frida
[+] Select the architecture of your system: 
If you don't know run: adb shell getprop ro.product.cpu.abi

1) armeabi
2) arm64-v8a
3) x86
4) armeabi-v7a
5) x86_64
6) I don't know. Inject frida-gadget for all architectures (slower)

> 4

[+] In with library do you want to inject?: 
 
1) libshinobicharts-android.so
2) libmodpng.so
3) libpl_droidsonroids_gif.so
4) libmodft2.so
5) libjniPdfium.so
6) libmodpdfium.so

[+] Enter the number of the desired library: 
> 1
[+] Downloading and extracting frida gadget for: armeabi-v7a
[+] Injecting libgdgt.so into armeabi-v7a/libshinobicharts-android.so

[*] Removing old signature
[+] APK Building...
[+] SUCCESS!! Your new apk is : my_app.apk. Now you should sign it.

```

[![asciicast](https://asciinema.org/a/HEz43ylizrdbnYy1nchZ2Hdq6.svg)](https://asciinema.org/a/HEz43ylizrdbnYy1nchZ2Hdq6)

## Signing the APK

Generation of the keystore:

```
keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000

```

Signing:

```
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore my_app.apk alias_name
```

[![asciicast](https://asciinema.org/a/cg06L0t339riozJHTIDPpna86.svg)](https://asciinema.org/a/cg06L0t339riozJHTIDPpna86)

## Frida Injection

The first error is because frida needs to be executed when the app is opened.

[![asciicast](https://asciinema.org/a/ATS971gneg3AzMqKEKtZOFAZA.svg)](https://asciinema.org/a/ATS971gneg3AzMqKEKtZOFAZA)